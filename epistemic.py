# ##### SYNTAX #####
class Formula:
    def __repr__(self):
        return str(self)


class Atom(Formula):
    def __init__(self, name: str):
        self.name = name

    def __str__(self):
        return self.name


class Muddy(Atom):
    def __init__(self, i: int):
        super().__init__(f"m{i}")
        self.i = i

    def __call__(self, s):
        assert s
        while not isinstance(s[0], bool):
            s = s[-1]
        return s[self.i]


class And(Formula):
    def __init__(self, *formulas):
        self.formulas = formulas

    def __str__(self):
        return f"({'&'.join([str(formula) for formula in self.formulas])})"


class Neg(Formula):
    def __init__(self, formula: Formula):
        self.formula = formula

    def __str__(self):
        return f"~{self.formula}"


class K(Formula):
    def __init__(self, agent, formula: Formula):
        self.agent = agent
        self.formula = formula

    def __str__(self):
        return f"K{self.agent} {self.formula}"


class D(Formula):
    def __init__(self, announcement, formula: Formula):
        self.announcement = announcement
        self.formula = formula

    def __str__(self):
        return f"[{self.announcement}] {self.formula}"


def ou(a, b):
    return Neg(And(Neg(a), Neg(b)))


def impl(a, b):
    return ou(b, Neg(a))


def equiv(a, b):
    return And(impl(a, b), impl(b, a))


def dp(a, p):
    return Neg(D(a, Neg(p)))


T = And([])


def simplify(p):
    # Removes double negations
    match p:
        case Atom():
            return p
        case And(formulas=formulas):
            return And(*map(simplify, formulas))
        case Neg(formula=formula):
            match formula:
                case Neg(formula=f):
                    return simplify(f)
                case _:
                    return Neg(simplify(formula))
        case K(agent=a, formula=formula):
            return K(agent=a, formula=simplify(formula))
        case D(announcement=announcement, formula=formula):
            return D(
                announcement=simplify(announcement), formula=simplify(formula)
            )


def depth(p):
    match p:
        case Atom():
            return 0
        case And(formulas=formulas):
            return max(depth(formula) for formula in formulas)
        case Neg(formula=formula):
            return depth(formula)
        case K(formula=formula):
            return 1 + depth(formula)
        case D(announcement=announcement, formula=formula):
            return depth(announcement) + depth(formula)


# ##### SEMANTICS #####
class Kripke:
    def __init__(self, states: set, relations: dict):
        self.states = states
        self.relations = relations

    def __call__(self, a, s):
        return self.relations[a][s]

    def reduce(self, ns: set, p=None):
        nr = {
            agent: {
                state: {s for s in self.relations[agent][state] if s in ns}
                for state in self.relations[agent]
                if state in ns
            }
            for agent in self.relations
        }
        return Kripke(states=ns, relations=nr)

    def __len__(self):
        return len(self.states)


def check(m, s, p):
    match p:
        case Atom():
            return p(s)
        case And(formulas=formulas):
            return all(check(m, s, formula) for formula in formulas)
        case Neg(formula=formula):
            return not check(m, s, formula)
        case K(agent=a, formula=formula):
            return all(check(m, sp, formula) for sp in m(a, s))
        case D(announcement=announcement, formula=formula):
            ns = set(s for s in m.states if check(m, s, announcement))
            return (s not in ns) or check(m.reduce(ns), s, formula)


def removed(m, s, p):
    match p:
        case Atom():
            return 0
        case And(formulas=formulas):
            return max(removed(m, s, formula) for formula in formulas)
        case Neg(formula=formula):
            return removed(m, s, formula)
        case K(agent=a, formula=formula):
            return max(removed(m, sp, formula) for sp in m(a, s))
        case D(announcement=announcement, formula=formula):
            ns = set(s for s in m.states if check(m, s, announcement))
            if s not in ns:
                return len(m.states) - len(ns)
            return len(m.states) - len(ns) + removed(m.reduce(ns), s, formula)


def visited(m, s, p):
    match p:
        case Atom():
            return 1
        case And(formulas=formulas):
            return max(visited(m, s, formula) for formula in formulas)
        case Neg(formula=formula):
            return visited(m, s, formula)
        case K(agent=a, formula=formula):
            return len(m(a, s)) + sum(visited(m, sp, formula) for sp in m(a, s))
        case D(announcement=announcement, formula=formula):
            t = visited(m, s, announcement)
            ns = set(s for s in m.states if check(m, s, announcement))
            if s not in ns:
                return t
            return t + visited(m.reduce(ns), s, formula)
