from __future__ import annotations

from epistemic import Formula, Atom, And, K, Neg, D, depth, simplify, Kripke, dp, ou
from muddy import n, k, r, s, states, omega, Muddy


# ##### SYNTAX #####
class Exact(Atom):
    def __init__(self, a: int, d: int):
        super().__init__(f"E({a},{d})")
        self.a = a
        self.d = d


class AtLeast(Atom):
    def __init__(self, a: int, d: int):
        super().__init__(f"P({a},{d})")
        self.a = a
        self.d = d


# ##### SEMANTICS #####
class KripkeDepth(Kripke):
    def __init__(self, states: set, relations: dict, depths):
        super().__init__(states, relations)
        self.depths = depths


class KripkeS1(KripkeDepth):
    def reduce(self, ns: set, p: Formula):
        d = depth(p)

        nd = {
            a: {s: self.depths[a][s] - d for s in self.depths[a]}
            for a in self.depths
        }

        nr = {
            agent: {
                state: {s for s in self.relations[agent][state] if s in ns}
                for state in self.relations[agent]
                if state in ns
            }
            for agent in self.relations
        }

        return KripkeS1(states=ns, relations=nr, depths=nd)

    # Restrict to sets with no modal operation
    def trim(self, ns: set):
        nr = {
            agent: {
                state: {s for s in self.relations[agent][state] if s in ns}
                for state in self.relations[agent]
                if state in ns
            }
            for agent in self.relations
        }
        return KripkeS1(states=ns, relations=nr, depths=self.depths)


class KripkeS2(KripkeDepth):
    def reduce(self, ns: set, p: Formula):
        d = depth(p)

        nd = {
            a: {
                s: self.depths[a][s] - d if self.depths[a][s] >= d else
                self.depths[a][s]
                for s in self.depths[a]}
            for a in self.depths
        }

        nr = {
            agent: {
                state: {s for s in self.relations[agent][state] if
                        self.depths[agent][state] < d or (state in ns and s in ns)
                        or (state not in ns and s not in ns)}
                for state in self.relations[agent]
            }
            for agent in self.relations
        }

        return KripkeS2(states=states, relations=nr, depths=nd)

    # Restrict to sets with no modal operation
    def trim(self, ns: set):
        nr = {
            agent: {
                state: {s for s in self.relations[agent][state] if s in ns}
                for state in self.relations[agent]
                if state in ns
            }
            for agent in self.relations
        }
        return KripkeS2(states=ns, relations=nr, depths=self.depths)


class KripkeS3(KripkeDepth):
    def reduce(self, ns: set, p: Formula):
        d = depth(p)

        nns = set((0, s) for s in self.states) | set((1, s) for s in ns)

        nd = {
            a: {(0, s): self.depths[a][s] for s in self.depths[a]} | {
                (1, s): self.depths[a][s] - d if self.depths[a][s] >= d else
                self.depths[a][s] for s in self.depths[a]}
            for a in self.depths
        }

        nr = {
            agent: {
                (0, state):
                    {(0, s) for s in self.relations[agent][state]} |
                    ({(1, s) for s in self.relations[agent][state] & ns}
                     if any(self.depths[agent][s] < d for s in self.relations[agent][state] & ns)
                     else set())
                for state in self.relations[agent]
            } | {
                (1, state):
                    {(1, s) for s in set(self.relations[agent][state]) & ns} |
                    ({(0, s) for s in self.relations[agent][state]}
                     if any(self.depths[agent][s] < d for s in self.relations[agent][state] & ns)
                     else set())
                for state in self.relations[agent]
            }
            for agent in self.relations
        }

        return KripkeS3(states=nns, relations=nr, depths=nd)

    # Restrict to sets with no modal operation
    def trim(self, ns: set):
        nr = {
            agent: {
                state: {s for s in self.relations[agent][state] if s in ns}
                for state in self.relations[agent]
                if state in ns
            }
            for agent in self.relations
        }
        return KripkeS3(states=ns, relations=nr, depths=self.depths)


def check(m, s, p):
    match p:
        case Exact(a=a, d=d):
            return m.depths[a][s] == d
        case AtLeast(a=a, d=d):
            return m.depths[a][s] >= d
        case Atom():
            return p(s)
        case And(formulas=formulas):
            return all(check(m, s, formula) for formula in formulas)
        case Neg(formula=formula):
            return not check(m, s, formula)
        case K(agent=a, formula=formula):
            d = depth(formula)
            return check(m, s, AtLeast(a, d)) and all(
                check(m, sp, formula) for sp in m(a, s))
        case D(announcement=announcement, formula=formula):
            ns = set(s for s in m.states if check(m, s, announcement))
            if s not in ns:
                return True
            if isinstance(m, KripkeS3):
                s = (1, s)
            return check(m.reduce(ns=ns, p=announcement), s, formula)


def needed(p, k=0):
    match p:
        case Atom():
            return p
        case And(formulas=formulas):
            return And(*(needed(formula, k=k) for formula in formulas))
        case Neg(formula=formula):
            return Neg(needed(formula, k=k))
        case K(agent=a, formula=formula):
            return And(AtLeast(a, depth(formula)+k), K(a, needed(formula, k=k)))
        case D(announcement=announcement, formula=formula):
            return D(needed(announcement, k=k),
                     needed(formula, k=k+depth(announcement)))


# ##### MODEL #####
depths = {a: {s: k-1-a for s in states} for a in range(n)}
# depths = {a: {s: k-1-a for s in states} for a in range(n)}
# print(depths)
ms1 = KripkeS1(states=states, relations=r, depths=depths)
ms2 = KripkeS2(states=states, relations=r, depths=depths)
ms3 = KripkeS3(states=states, relations=r, depths=depths)
ms1 = ms1.trim(states)
ms2 = ms2.trim(states)
ms3 = ms3.trim(states)

if __name__ == "__main__":
    print(f"{n=} {k=} {len(states)=}")

    f = simplify(omega(k, d=1))
    print(f)
    fn = simplify(needed(f))
    print(fn)
    print(check(ms1, s, f))
    print(check(ms2, s, f))
    print(check(ms3, s, f))

    f = simplify(dp(Neg(K(2, Muddy(2))), dp(Neg(K(1, Muddy(1))),
                                        Neg(K(2, ou(Muddy(0), Neg(Muddy(0))))))))
    print(f)
    print(check(ms1, s, f))
    print(check(ms2, s, f))
    print(check(ms3, s, f))

    f = simplify(dp(K(1, Neg(K(2, Muddy(2)))), K(1, K(0, Muddy(0)))))
    print(f)
    print(check(ms1, s, f))
    print(check(ms2, s, f))
    print(check(ms3, s, f))


    # To be tested with d(0, \cdot) = 1.
    f = simplify(dp(K(1, Neg(K(2, Muddy(2)))), K(0, Muddy(0))))
    print(f)
    print(check(ms1, s, f))
    print(check(ms2, s, f))
    print(check(ms3, s, f))
