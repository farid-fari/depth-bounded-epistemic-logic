from itertools import product

from epistemic import (
    Kripke, K, Muddy, dp, Neg, simplify, depth, removed, visited, check, And
)

# ##### MODEL #####
n = 3
k = 3
assert 1 <= k <= n

states = set(product({False, True}, repeat=n))
states.remove((False,) * n)
s = tuple([True] * k + [False] * (n - k))
assert sum(s) == k and len(s) == n


def rev(s, k):
    return tuple(t if i != k else not t for i, t in enumerate(s))


r = {a: {cs: {cs, rev(cs, a)} for cs in states} for a in range(n)}

m = Kripke(states=states, relations=r)
m = m.reduce(states)


# ##### FORMULAS #####
def base(k):
    # Is the base announcements from vanilla muddy children
    if k == 1:
        return K(0, Muddy(0))
    nobody = And(*(
        And(Neg(K(i, Muddy(i))), Neg(K(i, Neg(Muddy(i)))))
        for i in range(n)
    ))
    return dp(nobody, base(k-1))


def phi(k):
    # Corresponds to omega(k=k, d=1)
    if k == 1:
        return K(0, Muddy(0))
    return dp(Neg(K(k - 1, Muddy(k - 1))), phi(k - 1))


def psi(k):
    # Corresponds to omega(k=k, d=2)
    match k:
        case 1:
            return K(0, Muddy(0))
        case 2:
            return dp(Neg(K(1, Muddy(1))), psi(1))
        case k:
            return dp(K(k - 1, Neg(K(k - 2, Muddy(k - 2)))), psi(k - 1))


def xsi(k):
    # Corresponds to omega(k=k, d=3)
    match k:
        case 1:
            return K(0, Muddy(0))
        case 2:
            return dp(Neg(K(1, Muddy(1))), psi(1))
        case 3:
            return dp(K(2, Neg(K(1, Muddy(1)))), psi(2))
        case k:
            return dp(
                K(k - 1, K(k - 2, Neg(K(k - 3, Muddy(k - 3))))), xsi(k - 1)
            )


def omega(k, d):
    def _build_to(c, t):
        match t:
            case 0:
                return Neg(K(c, Muddy(c)))
            case t:
                return K(c, _build_to(c-1, t-1))

    match k:
        case 1:
            return K(0, Muddy(0))
        case k if 2 <= k <= d:
            return omega(1, d)
            # return dp(_build_to(c=k-1, t=k-2), omega(k-1, d))
        case k:
            return dp(_build_to(c=k-1, t=d-1), omega(k-1, d))


if __name__ == "__main__":
    import math
    import sys

    sys.setrecursionlimit(5000)
    # Sanity check
    # print(simplify(phi(k=k)))
    # print(simplify(omega(k=k, d=1)))
    # print(simplify(psi(k=k)))
    # print(simplify(omega(k=k, d=2)))
    # print(simplify(xsi(k=k)))
    # print(simplify(omega(k=k, d=3)))

    print(f"{n=} {k=} {len(states)=}")

    formulas = [simplify(omega(k=k, d=d)) for d in range(1, k)]
    depths = [depth(f) for f in formulas]

    truth = [check(m, s, f) for f in formulas]
    loss = [removed(m, s, f) for f in formulas]
    vis = [visited(m, s, f) for f in formulas]

    assert all(truth)

    print(depths)
    print(loss)
    print("true loss", sum(math.comb(n, t) for t in range(1, k)))
    print(vis)

    print()
    list(map(print, formulas))

    # f = dp(Neg(K(2, Muddy(2))), dp(K(1, Muddy(1)), K(3, Neg(Muddy(3)))))
    # print(simplify(f))
    # print(check(m, s, f))
