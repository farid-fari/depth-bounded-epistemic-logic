#!/usr/bin/env python
import bounded
import epistemic
from epistemic import Atom, And, dp, Muddy, K, Neg, D, simplify
from muddy import n, k, s, m, base, omega
from bounded import Exact, ms1, ms2, KripkeS3, KripkeS1, ms3

import networkx as nx
import numpy as np
import matplotlib.pyplot as plt


def visualize(m, s, p, check):
    match p:
        case Atom():
            return []
        case Neg(formula=formula):
            return visualize(m, s, formula, check=check)
        case K(formula=formula):
            return visualize(m, s, formula, check=check)
        case D(announcement=announcement, formula=formula):
            ns = set(s for s in m.states if check(m, s, announcement))
            assert s in ns
            if isinstance(m, KripkeS3):
                s = (1, s)
            nm = m.reduce(ns=ns, p=announcement)
            return [(s, nm)] + visualize(nm, s, formula, check=check)


def plot_formula(m, f, ax, name, check):
    ss = [(s, m)] + visualize(m, s, f, check=check)
    assert len(ss) <= k
    st = {s for s in m.states if check(m, s, f)}
    for i, (_, mod) in enumerate(ss):
        cs = mod.states
        cst = st & cs
        nx.draw_networkx_nodes(
            g, pos=pos, node_color=[(0., 1., 0., .6) for _ in cst], node_size=150,
            nodelist=cst, ax=ax[i])
        nx.draw_networkx_nodes(
            g, pos=pos, node_size=150, nodelist=cs - cst, ax=ax[i])
        nx.draw_networkx_nodes(
            g, pos=pos, node_color="green", nodelist=[s], node_size=150, ax=ax[i])
        nx.draw_networkx_edges(
            g, pos=pos, edge_color="red", width=1.7, edgelist=[
                (u, v) for u, v, e in g.edges(data=True) if e['agent'] == 0
                and u in cs and v in cs and v in mod(0, u)
            ], ax=ax[i])
        nx.draw_networkx_edges(
            g, pos=pos, edgelist=list(set([
                (u, v) for u, v, e in g.edges(data=True) if e['agent'] != 0
                and u in cs and v in cs and v in mod(e['agent'], u)
            ])), ax=ax[i])
        ax[i].set_title(f"{name} ({len(cs)} states)")

    if isinstance(m, KripkeS1):
        print({a: ss[-1][-1].depths[a][s] for a in range(n)})


def plot_formula3(f, ax, name):
    ss = [(s, ms3)] + visualize(ms3, s, f, check=bounded.check)
    cpos = pos
    assert len(ss) <= k

    for i, (cus, mod) in enumerate(ss):
        g = nx.Graph()
        g.add_edges_from([
            (s1, s2, {"agent": a, "weight": .5 + .5*(a != 0)})
            for a in mod.relations for s1 in mod.relations[a]
            for s2 in mod.relations[a][s1] if s1 != s2])

        cs = mod.states & set(nx.node_connected_component(g, cus))
        nx.draw_networkx_nodes(g, nodelist=cs, pos=cpos, node_size=150, ax=ax[i])
        nx.draw_networkx_nodes(
            g, pos=cpos, node_color="green", nodelist=[cus], node_size=150, ax=ax[i])
        nx.draw_networkx_edges(
            g, pos=cpos, edge_color="red", width=1.7, edgelist=[
                (u, v) for u, v, e in g.edges(data=True) if e['agent'] == 0
                and u in cs and v in cs and v in mod(0, u)
            ], ax=ax[i])
        nx.draw_networkx_edges(
            g, pos=cpos, edge_color=(0., 0., 0., .7), edgelist=[
                (u, v) for u, v, e in g.edges(data=True) if e['agent'] != 0
                and u in cs and v in cs and v in mod(e['agent'], u)
            ], ax=ax[i])
        ax[i].set_title(f"{name} ({len(cs)} states)")

        delta = 4
        if i == 0:
            cpos = {(1, s): cpos[s] + (delta, 0) for s in cpos} | {
                    (0, s): cpos[s] for s in cpos}
        elif i == 1:
            cpos = {(1, s): cpos[s] + (0, delta) for s in cpos} | {
                    (0, s): cpos[s] for s in cpos}


def base_f(k, f):
    # Tests f after the announcements from vanilla muddy children
    if k == 1:
        return f
    nobody = And(*(
        And(Neg(K(i, Muddy(i))), Neg(K(i, Neg(Muddy(i)))))
        for i in range(n)
    ))
    return dp(nobody, base_f(k-1, f))


dmax = 3

g = nx.Graph()
g.add_edges_from([
    (s1, s2, {"agent": a, "weight": .5 + .5*(a != 0)})
    for a in m.relations for s1 in m.relations[a]
    for s2 in m.relations[a][s1] if s1 != s2])
# pos = nx.spring_layout(g, weight='weight', seed=0)
pos = {s: np.array(
    [int(k) for k in s[:2]]) + ((.5, .5) if s[2] else (0, 0)) for s in m.states}

if __name__ == "__main__":
    fig, axs = plt.subplots(nrows=dmax+1, ncols=k)
    fig.tight_layout()

    f = simplify(base(k=k))
    # f = simplify(omega(k=k, d=2))
    print(f)

    print(epistemic.check(m, s, f))
    plot_formula(m, f, axs[0], name="PAL", check=epistemic.check)

    print(bounded.check(ms3, s, f))
    plot_formula3(f, axs[1], name="DPAL")

    print(bounded.check(ms1, s, f))
    plot_formula(ms1, f, axs[2], name="Semantics 1", check=bounded.check)

    print(bounded.check(ms2, s, f))
    plot_formula(ms2, f, axs[3], name="Semantics 2", check=bounded.check)

    plt.show()
