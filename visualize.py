#!/usr/bin/env python
from epistemic import Atom, And, K, Neg, D, depth, simplify, check
from muddy import n, k, s, m, omega, states, base

import networkx as nx
import matplotlib.pyplot as plt


def visualize(m, s, p, i=0):
    match p:
        case Atom():
            return []
        case Neg(formula=formula):
            return visualize(m, s, formula)
        case K(formula=formula):
            return visualize(m, s, formula)
        case D(announcement=announcement, formula=formula):
            ns = set(s for s in m.states if check(m, s, announcement))
            assert s in ns
            return [ns] + visualize(m.reduce(ns), s, formula)


def plot_formula(f, ax, d):
    ss = [m.states] + visualize(m, s, f)
    assert len(ss) <= k
    st = {s for s in m.states if check(m, s, f)}
    for i, cs in enumerate(ss):
        cst = st & cs
        nx.draw_networkx_nodes(
            g, pos=pos, node_color=[(0., 1., 0., .6) for _ in cst],
            nodelist=cst, ax=ax[i])
        nx.draw_networkx_nodes(
            g, pos=pos, node_color=[(0., 0., 1., .6) for _ in cs - cst],
            nodelist=cs - cst, ax=ax[i])
        nx.draw_networkx_nodes(
            g, pos=pos, node_color="green", nodelist=[s], ax=ax[i])
        nx.draw_networkx_edges(
            g, pos=pos, edge_color="red", width=1.7, edgelist=[
                (u, v) for u, v, e in g.edges(data=True) if e['agent'] == 0
                and u in cs and v in cs
            ], ax=ax[i])
        nx.draw_networkx_edges(
            g, pos=pos, edge_color=(0., 0., 0., 0.7), edgelist=[
                (u, v) for u, v, e in g.edges(data=True) if e['agent'] != 0
                and u in cs and v in cs
            ], ax=ax[i])
        ax[i].set_title(f"{d+1} {len(cs)=}")


dmax = k - 1

g = nx.Graph()
g.add_edges_from([
    (s1, s2, {"agent": a, "weight": .5 + .5*(a != 0)})
    for a in m.relations for s1 in m.relations[a]
    for s2 in m.relations[a][s1] if s1 != s2])
pos = nx.spring_layout(g, weight='weight', seed=0)
fig, axs = plt.subplots(nrows=dmax+1, ncols=k)

f = simplify(base(k=k))
print(-1, f, check(m, s, f))
plot_formula(f, axs[0], -1)

for d in range(dmax):
    f = simplify(omega(k=k, d=d+1))
    print(d, f, check(m, s, f))
    plot_formula(f, axs[d+1], d)

plt.show()
